﻿using System;

namespace HW2
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new db.AvitoContext();
            //Заполнение БД. Вызывается 1 раз
            //db.FillDb.FillData(context); 
           
            ConsoleUI.MakeInteractiveDialog(context);

        }
    }
}
