﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HW2
{
    public static class ConsoleUI
    {
        private static void MakeNewYearThree(int n)
        {
            var prevColor = Console.ForegroundColor;
            for (int i = 1; i <= n; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    string branch = new string('*', j);
                    Console.ForegroundColor = ConsoleColor.Green;
                    string res = string.Empty;
                    for (int k = 0; k < 2; k++)
                    {
                        res += branch.PadLeft(n * k + 15 - j * k) + "*" + branch;
                    }
                    Console.WriteLine(res);
                }
            }
            Console.ForegroundColor = prevColor;

        }

        public static void MakeInteractiveDialog(db.AvitoContext dbContext)
        {
            MakeNewYearThree(5);
            ShowMainMenu();
            while (InterpritateCommand(Console.ReadKey().KeyChar, dbContext))
            {
                Console.WriteLine("Нажмите любую кнопку для продолжения");
                Console.ReadKey();
                Console.Clear();
                ShowMainMenu();

            }
        }

        public static void ShowMainMenu()
        {
            Console.WriteLine(@"Введите номер команды для выполнения: 
                                1. Вывод всех таблиц.
                                2. Добавление данных.
                                3. Заполнение таблиц начальными данными.
                                0. Завершение работы");
        }

        public static bool InterpritateCommand(char key, db.AvitoContext dbContext)
        {
            switch (key)
            {
                case '0':
                    return false;
                case '1':
                    {
                        db.ShowDb.ShowAllTables(dbContext);
                        break;
                    }
                case '2':
                    {
                        AddTableItemSubmenu();
                        while (InterpritateAddSubmenuCommand(Console.ReadKey().KeyChar, dbContext))
                        {
                            Console.WriteLine("Нажмите любую кнопку для продолжения");
                            Console.ReadKey();
                            Console.Clear();
                            AddTableItemSubmenu();
                        }

                        break;
                    }
                case '3':
                    {
                        db.FillDb.FillData(dbContext);
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Ошибочная команда. Повторите ввод");
                        break;
                    }
            }
            return true;
        }

        public static void AddTableItemSubmenu()
        {
            Console.WriteLine(@"
                                Выберите таблицу для добавления данных:
                                1. Категории объявлений.
                                2. Пользователи.
                                3. Объявления. 
                                0. Завершение добавления данных");
        }

        public static bool InterpritateAddSubmenuCommand(char key, db.AvitoContext dbContext)
        {
            switch (key)
            {
                case '0':
                    return false;
                case '1':
                    {
                        db.AdType adt = new db.AdType();
                        FillFields(adt, dbContext);
                        dbContext.AdTypes.Add(adt);
                        break;
                    }
                case '2':
                    {
                        db.User usr = new db.User();
                        FillFields(usr, dbContext);
                        dbContext.Users.Add(usr);
                        break;
                    }
                case '3':
                    {
                        db.Ad ad = new db.Ad();
                        FillFields(ad, dbContext);
                        dbContext.Ads.Add(ad);
                        break;
                    }
                default:
                    {
                        Console.WriteLine("Ошибочная команда. Повторите ввод");
                        return true;
                    }
            }
            dbContext.SaveChanges();
            return true;
        }

        public static void FillFields<T>(T tableItem, db.AvitoContext dbContext) where T : class
        {
            foreach (PropertyInfo propertyInfo in tableItem.GetType().GetProperties())
            {
                object[] attribute = propertyInfo.GetCustomAttributes(typeof(db.InputAttribute), true);
                if (attribute.Length > 0)
                {
                    db.InputAttribute myAttribute = (db.InputAttribute)attribute[0];
                    if (myAttribute.Required)
                    {
                        if (!myAttribute.Referenced)
                        {
                            Console.Write(myAttribute.RussianPromt);
                            var val = Console.ReadLine();
                            var res = Cast(val, propertyInfo.PropertyType);
                            propertyInfo.SetValue(tableItem, res);
                        }
                        else
                        {
                            Console.WriteLine("\r\n Допустимые значения: ");
                            var cnst = propertyInfo.GetValue(tableItem);
                            GetPossibleValues((dynamic)cnst, dbContext);
                            Console.Write("\r\n {0}", myAttribute.RussianPromt);
                            int ind = int.Parse(Console.ReadLine());
                            var res = GetValue(ind, dbContext, (dynamic) cnst);
                            propertyInfo.SetValue(tableItem, res);
                        }
                    }
                }
            }
        }

        public static dynamic Cast(dynamic obj, Type castTo)
        {
            return Convert.ChangeType(obj, castTo);
        }

        public static void GetPossibleValues <T> (T constantValue, db.AvitoContext dbContext) where T: class
        {
            var someDbSetItems = dbContext.Set<T>();
            foreach (var item in someDbSetItems)
            {
                Console.WriteLine(item.ToString());
            }
           
        }

        public static T GetValue <T> (int id, db.AvitoContext dbContext, T val) where T : class
        {
            var someDbSetItems = dbContext.Set<T>();
            return someDbSetItems.Find(id);
        }
    }
}
