﻿using System;

namespace HW2.db
{
    public class InputAttribute: Attribute
    {
        public string RussianPromt { get; set; }

        public bool Required { get; set; }

        public bool Referenced { get; set; }
    }
}
