﻿using System;

namespace HW2.db
{
    public class Ad
    {
        public int Id { get; set; }

        [Input(RussianPromt = "Тип объявления: ", Required = true, Referenced = true)]
        public AdType AdType { get; set; }

        [Input(RussianPromt = "Текст объявления: ", Required = true)]
        public string AdText { get; set; }

        [Input(RussianPromt = "Дата публикации объявления: ", Required = true)]
        public DateTime Published { get; set; }

        [Input(RussianPromt = "Опубликовавший пользователь: ", Required = true, Referenced = true)]
        public User PublishedUser { get; set; }

        public Ad()
        {
            this.AdType = new AdType();
            this.PublishedUser = new User();
        }
    }
}
