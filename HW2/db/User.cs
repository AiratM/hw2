﻿using System.Collections.Generic;


namespace HW2.db
{
    /// <summary> Пользователи </summary>
    public class User
    {
        public int Id { get; set; }
        [Input (RussianPromt = "ФИО пользователя: ", Required = true)]
        public string FullName { get; set; }
        
        [Input(RussianPromt = "Email пользователя: ", Required =true)]
        public string Email { get; set; }

        [Input(RussianPromt = "Номер телефона пользователя: ", Required =true)]
        public string PhoneNumber { get; set; }

        [Input(RussianPromt = "Пароль пользователя: ", Required = true)]
        public string Password { get; set; }
        public List<Ad> Ads { get; set; }

        public override string ToString()
        {
            return $"{FullName}, email: {Email}, phone: {PhoneNumber}";
        }
    }
}
