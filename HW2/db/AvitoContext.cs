﻿using Microsoft.EntityFrameworkCore;

namespace HW2.db
{
    public class AvitoContext: DbContext
    {
        public DbSet<AdType> AdTypes { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Ad> Ads { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=localhost;Database=avito;Username=postgres;Password=dataR00t");
    }
}
