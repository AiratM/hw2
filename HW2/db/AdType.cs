﻿using System.Collections.Generic;


namespace HW2.db
{
    /// <summary> Типы объявлений </summary>
    public class AdType
    {
        public int Id { get; set; }

        [Input(RussianPromt = "Тип объявления: ", Required = true)]
        public string AdTypeName { get; set; }
        public List<Ad> ads { get; set; }
        public override string ToString()
        {
            return $"[{Id}] : {AdTypeName}";
        }
    }
}
