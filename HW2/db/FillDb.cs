﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW2.db
{
    public class FillDb 
    {
        public static void FillData(AvitoContext context)
        {
            var adTypes = new AdType[]
           {
               new AdType {AdTypeName = "Авто"},
               new AdType {AdTypeName = "Недвижимость"},
               new AdType {AdTypeName = "Услуги"},
               new AdType {AdTypeName = "Электроника"},
               new AdType {AdTypeName = "Прочее"}
           };
            context.AdTypes.AddRange(adTypes);
            Console.WriteLine("Добавлены базовые типы объявлений");

            var users = new User[]
            {
                new User{Email = "IvanovII@example.com", FullName="Иванов Иван Иванович",Password="pass123",PhoneNumber="+79123456789"},
                new User{Email = "PetrovII@example.com", FullName="Петров Иван Иванович",Password="pass1234",PhoneNumber="+79123456790"},
                new User{Email = "PetrovPI@example.com", FullName="Петров Петр Иванович",Password="pass12345",PhoneNumber="+79123456791"},
                new User{Email = "PetrovPP@example.com", FullName="Петров Петр Петрович",Password="pass123456",PhoneNumber="+79123456792"},
                new User{Email = "SidorovIP@example.com", FullName="Сидоров Иван Петрович",Password="pass12354",PhoneNumber="+79123456793"}

            };
            context.Users.AddRange(users);
            Console.WriteLine("Добавлены тестовые пользователи");

            var ads = new Ad[] 
            { 
                new Ad{AdText = "Продам авто. Не битый, не крашеный", Published =DateTime.Today, AdType = adTypes[0], PublishedUser = users[0] },
                new Ad{AdText = "Продам дом. Недорого", Published =DateTime.Today, AdType = adTypes[1], PublishedUser = users[1] },
                new Ad{AdText = "Автоперевозки 24/7", Published =DateTime.Today, AdType = adTypes[2], PublishedUser = users[2] },
                new Ad{AdText = "Продам монитор, 23 дюйма, в рабочем состоянии", Published =DateTime.Today, AdType = adTypes[3], PublishedUser = users[3] },
                new Ad{AdText = "Отдам котят в добрые руки", Published =DateTime.Today, AdType = adTypes[4], PublishedUser = users[4] }
            };
            context.Ads.AddRange(ads);
            Console.WriteLine("Добавлены тестовые объявления");
            context.SaveChanges();
            Console.WriteLine("Изменения сохранены");
        }

    }
}
