﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HW2.db
{
    public class ShowDb
    {
        public static void ShowAllTables(AvitoContext context)
        {
            Console.WriteLine("Таблица Типы объявлений");
            var adTypes = context.AdTypes;
            foreach (AdType adType in adTypes)
                Console.WriteLine($"ID: {adType.Id}; Тип объявления: {adType.AdTypeName}");
            
            Console.WriteLine("Таблица Пользователи");
                        var adUsers = context.Users;
            foreach (User adUser in adUsers)
                Console.WriteLine($"ID: {adUser.Id}; ФИО: {adUser.FullName}; Email: {adUser.Email}; Телефон: {adUser.PhoneNumber}");

            Console.WriteLine("Таблица Объявления");
            var adList = context.Ads;
            foreach (Ad ad in adList)
                Console.WriteLine($"ID: {ad.Id}, текст: {ad.AdText}, тип объявления: {ad.AdType}, опубликовано: {ad.Published.ToString("dd.MM.yyyy")}, автор: {ad.PublishedUser}");
        }
    }
}
