﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HW2.Migrations
{
    public partial class ChangedUserField : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ads_Users_UserId",
                table: "Ads");

            migrationBuilder.RenameColumn(
                name: "UserId",
                table: "Ads",
                newName: "PublishedUserId");

            migrationBuilder.RenameIndex(
                name: "IX_Ads_UserId",
                table: "Ads",
                newName: "IX_Ads_PublishedUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Ads_Users_PublishedUserId",
                table: "Ads",
                column: "PublishedUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Ads_Users_PublishedUserId",
                table: "Ads");

            migrationBuilder.RenameColumn(
                name: "PublishedUserId",
                table: "Ads",
                newName: "UserId");

            migrationBuilder.RenameIndex(
                name: "IX_Ads_PublishedUserId",
                table: "Ads",
                newName: "IX_Ads_UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Ads_Users_UserId",
                table: "Ads",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
